<?php

/*
Plugin Name: Social Icon.
 */

add_action( 'admin_menu', function(){
	add_menu_page( 'Настройка иконок соц.сетей', 'Социальные сети', 8 , __FILE__ , 'show_social_option' );
});

function show_social_option(){ 

  $presettings = '';
  if(isset($_POST['hidden']) && empty($presettings)){
          $presettings = $_POST['presocial'];      
          update_option('presocial', $presettings);
  } 
  $presettings = get_option('presocial');
  $socials = explode(',',$presettings);?>

  <div class="wrap">
    <form name="presocial" method="POST">
      <table class="form-table">
        <tbody>
          <tr>
            <th ><label>Добавьте названия социальных сетей на английском языке через ","</label></th>
            <td>
              <textarea name="presocial"><?=$presettings?></textarea>
            </td>
          </tr>    
        </tbody>
      </table>
      <input type="hidden" name="hidden">
      <input type="submit" class="button button-primary button-large" value="Обновить настройки">
      <?php wp_nonce_field(); ?>
    </form>
  </div> 
  <?php

	$fields = [
	    [
        'label' => 'Ссылка',
        'name' => '_link',
        'value' => null,
        'type' => 'text'
	    ],
	    // [
     //    'label' =>'Сохранять настройку',
     //    'name' => '_checkbox',
     //    'value' => 5,
     //    'type' => 'checkbox'
	    // ],
	];

	$settings = [];

	if(isset($_POST['hidden'])){
        foreach ($_POST as $field => $value){
        $settings[ $field ] = $value;       
        }
		update_option('social', $settings);
	}	

	$settings = get_option('social');
  //echo '<pre>'; print_r($settings); echo '</pre>';
	?>
	<div class="wrap">
  	<h1>Настройки блока соц.сетей</h1>
  	<form name="social" method="POST">
  		<table class="form-table">
  			<tbody>   
      <?php foreach ($socials as $social){ 
              $social = trim($social);?>
              <tr>
                  <th >Социальная сеть:</th>
                  <td><b><?=$social?></b></td>
              </tr> 

  		<?php foreach ($fields as $field){

      			    $field['value'] = $settings[$social.$field['name']];
      			    // echo '<pre>'; print_r($settings[$field['name']]); echo '</pre>';?>
      			    <tr>
      			        <th ><label><?= $field['label'] ?></label></th>
      			        <td>
                        <input type="<?=$field['type']?>" name="<?=$social?><?=$field['name']?>" value="<?=$field['value']?>">
                    </td>
      			    </tr>    
        <?php }
            }?>
  			</tbody>
  		</table>
  		<input type="hidden" name="hidden">
      <input type="hidden" name="presocial" value="<?=$presettings?>">
  		<input type="submit" class="button button-primary button-large" value="Обновить настройки">
  		<?php wp_nonce_field(); echo '<pre>';
      // print_r($_POST); echo '</pre>';?>
  	</form>

	</div>			
<?php
}
	
class Social extends WP_Widget
{
  public function __construct() {
      parent::__construct("TextWidget", "Иконки соц.сетей",
          array("description" => "Виджет для вывода"));
  }

  public function form($instance) {
    $view = "";
    if (!empty($instance)) {
      $view = $instance["view"];
    }

    $viewId = $this->get_field_id("view");
    $viewName = $this->get_field_name("view");
    echo '<p>Режим отображения:  </p>';
    echo '<label><input  id="' . $viewId . '" type="radio" name="' . $viewName . '" value="header"';if($instance["view"] == "header")echo 'checked="checked"';echo'> для шапки сайта </label>';

    echo '<label><input  id="' . $viewId . '" type="radio" name="' . $viewName . '" value="contact"'; if($instance["view"] == "contact")echo 'checked="checked"';echo'> для страницы контактов </label><br><br>';
    return $categoryId;
  }

  public function update($newInstance, $oldInstance) {
    $values = array(); 
    $values["view"] = $newInstance["view"]; 
    return $values;
  }

  public function widget($args, $instance) {
  	$settings = get_option('social');
    $settings = array_slice($settings,0,-4); 
    // $settings = array_chunk($settings,2,true);?>
    <?= $args['before_widget'] ?>
      <?php 
        $n = 0;
        // foreach ( $settings as $setting)
          foreach ($settings as $key => $set){ $n++;
            //if ($n%2 != 0 && !empty($set[0]))
            if (!empty($set))
            {
              $newkey = explode('_' , $key);
              $newkey = $newkey[0];
            ?>
              <div class="social-icon">
                <a href="<?=trim($set);?>" target="_blank">

            <?php if($instance["view"]=='header'){ ?>
                  <img  src="<?php bloginfo('template_url');?>/images/<?=$newkey?>.png" alt="" width="32" height="32">
            <?php } else { ?>
                  <i class="gdlr-icon icon-<?=$newkey?> fa fa-<?=$newkey?>" style="color: #444444; font-size: 28px; float:left; margin:0 6px; "></i>
            <?php } ?>
                </a>
              </div>
      <?php }
          }
   
    echo $args['after_widget'];     
  }
}

add_action("widgets_init", function () {
    register_widget("social");
});




