<?php

/*
Plugin Name: Twitter parser
 */

add_action( 'init', 'register_post_type_twit' ); 
 
function register_post_type_twit() {
 $labels = array(
     'name' => 'Твиты',
     'add_new' => 'Добавить новый',
     'all_items' => 'Все твиты',
   );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'show_ui' => true, // показывать интерфейс в админке
    'has_archive' => true, 
    //'menu_icon' => get_stylesheet_directory_uri() .'/img/function_icon.png', // иконка в меню
    //'menu_icon' => '', // иконка в меню
    'menu_position' => 5, // порядок в меню
    'supports' => array( 'title', 'editor', 'comments', 'author', 'thumbnail')
  );
  register_post_type('twit', $args);
}

add_action( 'admin_menu', function(){
	add_menu_page( 'Добаление твиттов', 'Твиттер', 8 , __FILE__ , 'show_tweet_option' );
});

function show_tweet_option(){

	$fields = [
	    [
	        'label' => 'oauth_access_token',
	        'name' => 'secret_field1',
	        'value' => null,
	        'type' => 'text'
	    ],
	    [
	        'label' => 'oauth_access_token_secret',
	        'name' => 'secret_field2',
	        'value' => null,
	        'type' => 'text'
	    ],
	    [
	        'label' => 'consumer_key',
	        'name' => 'secret_field3',
	        'value' => null,
	        'type' => 'text'
	    ],
	    [
	        'label' => 'consumer_secret',
	        'name' => 'secret_field4',
	        'value' => null,
	        'type' => 'text'
	    ],
	    [
	        'label' => 'имя в Твиттере',
	        'name' => 'name',
	        'value' => null,
	        'type' => 'text'
	    ],
	    [
	        'label' => 'количество подгружаемых твиттов',
	        'name' => 'count',
	        'value' => null,
	        'type' => 'number'
	    ],
	    [
	        'label' => 'заголовок',
	        'name' => 'title',
	        'value' => null,
	        'type' => 'text'
	    ],
	    [
	        'label' => 'количество отображаемых твиттов',
	        'name' => 'show_count',
	        'value' => null,
	        'type' => 'number'
	    ],
	    [
	        'label' => 'интервал обновления, cек.',
	        'name' => 'interval',
	        'value' => null,
	        'type' => 'number'
	    ],
	];

	$settings = [];
	if(isset($_POST['hidden'])){
		foreach ($_POST as $field => $value){
		    $settings[ $field ] = $value;
		}
		update_option('tw', $settings);
	}	
	$settings = get_option('tw');
	?>
	<div class="wrap">
	<h1>Настройки Twitter parser</h1>
	<form name="twitters" method="POST">
		<table class="form-table">
			<tbody>
		<?php foreach ($fields as $field){
			    $field['value'] = $settings[ $field['name']] ;
			    //echo '<pre>'; print_r($settings[ $field['name']]); echo '</pre>';?>
			    <tr>
			        <th scope="row"><label><?= $field['label'] ?></label></th>
			        <td><input type="<?= $field['type'] ?>" name="<?= $field['name'] ?>" <?php if ( $field['type'] == 'number') echo 'min="1"'?> value="<?= $field['value'] ?>" required></td>
			    </tr> 
		<?php }?>
			</tbody>
		</table>
		<input type="hidden" name="hidden">
		<input type="submit" class="button button-primary button-large" value="Обновить настройки">
		<?php wp_nonce_field(); ?>
	</form>
	</div>			
<?php
}
	
class TwitWidget1 extends WP_Widget
{
  public function __construct() {
      parent::__construct("Social", "TwitWidget1",
          array("description" => "Виджет для показа твитов"));
  }
  public function widget($args, $instance) {

  	$settings = get_option('tw');?>

    <?= $args['before_widget'] ?>
      <?= $args['before_title'] ?><?= $settings['1title']; ?><?= $args['after_title'];  
      	
	$args_loop = array(
	'posts_per_page' => $settings['show_count'], 
	'post_type' => 'twit',
	'meta_key' => 'twID',
	'orderby' => 'meta_value_num', 
	'order' => 'DESC', 
	);?>
     <section id="content-section-9">
      	<div class="gdlr-color-wrapper  gdlr-show-all gdlr-skin-green-twitter" style="background-color: #72d5cd; padding-top: 50px; padding-bottom: 20px; ">
      		<div class="container">
      			<div class="gdlr-item-title-wrapper gdlr-item pos-center gdlr-twitter-title gdlr-nav-container ">
      				<div class="gdlr-item-title-head">
      					<i class="fa fa-angle-left icon-angle-left gdlr-flex-prev"></i>
      					<h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">
      						<i class="fa fa-twitter icon-twitter"></i>
      					</h3>
      					<i class="fa fa-angle-right icon-angle-right gdlr-flex-next"></i>
      					<div class="clear"></div>
      				</div>
      			</div>
      			<div class="gdlr-item gdlr-twitter-item">
      				<div class="flexslider" data-type="carousel" data-columns="1" data-nav-container="gdlr-twitter-item">
							<ul class="slides" style="width: 600%; margin-left: -2280px;">
					        <?php    
					          $q = new WP_Query($args_loop);    
					          if($q->have_posts()) {
					            while($q->have_posts()){ $q->the_post();
					            echo '<li style="width: 1110px; float: left; display: block;">';
					            	echo '<div class="gdlr-twitter">';
							              $twDate = get_post_meta($q->post->ID, "twDate", true);
							              $time = strtotime($twDate) ;            
							              $t = time()-$time;
							              if ( $t < 60 ){ 
							                $twDateFormat = $t.' сек.';
							              } elseif ( $t < 3600 && ((time()+3600*3)%(3600*24))>3600){ 
							                $twDateFormat = round($t/60); $twDateFormat .=' мин.';
							              } elseif ($t<((time()+3600*3)%(3600*24))){
							                $twDateFormat = date('G ч. i мин.', $t);
							              } elseif ($t<((time()+3600*3)%(3600*24*365))){
							                $twDateFormat = date('d M', $time+3600*3); 
							              } else {
							                $twDateFormat = date('d M Y', $time+3600*3);
							              }
					              		echo '<div class="gdlr-twitter-text" style="margin-bottom: -20px;">';
					              		the_content();
					              		echo'</div>';
					              		echo '<span class="gdlr-twitter-time">'.$twDateFormat.'</span>';
					                echo '</div>';
					            echo '</li>';
					            } 
					          }?>     
      						</ul>
  						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
	</section> 
    <?php 
    echo $args['after_widget'];     
  }

}

add_action("widgets_init", function () {
    register_widget("TwitWidget1");
});

//*************************************************************

add_filter( 'cron_schedules', 'my_interval1' );

function my_interval1( $schedules ) {
   $schedules['20seconds'] = array(
   'interval' => $settings['interval'], 
   'display'  => 'Каждые 20 секунд',
   );   

return $schedules;
}

if ( ! wp_next_scheduled('tweet_hook')) {
    wp_schedule_event( (time()), '20seconds', 'tweet_hook');
} 
 
add_action( 'tweet_hook', 'create_tweet1');

function create_tweet1() {

	$settings = get_option('tw');

  	require_once('TwitterAPIExchange.php');
  	$settingstw = array(
      'oauth_access_token' => $settings['secret_field1'],
      'oauth_access_token_secret' => $settings['secret_field2'],
      'consumer_key' => $settings['secret_field3'],
      'consumer_secret' => $settings['secret_field4']
 	);

  $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
  $getfield = '?screen_name='.$settings['name'].'&'.'count='.$settings['count'];
  $requestMethod = 'GET';
  $twitter = new TwitterAPIExchange($settingstw);

  $response = $twitter->setGetfield($getfield)
            ->buildOauth($url, $requestMethod)
            ->performRequest();  
  $responseDecode = array_reverse(json_decode($response));
  echo $responseDecode;

  $args_twit = array(
    'numberposts'      => 20,
    'post_type'        => 'twit',
  ); 
  $last_twit_posts = (wp_get_recent_posts( $args_twit ));
  $arrID = [];
  foreach ($last_twit_posts as $last_twit_post){

    	$arrID[] = (get_post_meta($last_twit_post['ID'],"twID")[0]);
  }
  	if(!empty ($arrID)) $last_twID = max($arrID);

  	foreach ($responseDecode as $resp) {  
	    if(($resp -> id_str) > $last_twID){  
			$post_data = array(
				'post_title'    => wp_strip_all_tags($resp -> user -> name),
				'post_content'  => $resp -> text,
				'post_status'   => 'publish',
				'post_type' => 'twit',                    
			);   
			$post_id = wp_insert_post( $post_data );
			add_post_meta($post_id, 'twID', $resp -> id_str,true);
			add_post_meta($post_id, 'twDate', $resp -> created_at,true);
			add_post_meta($post_id, 'avatarURL', $resp -> user -> profile_image_url,true);
			add_post_meta($post_id, 'imgURL', $resp -> entities -> media[0] -> media_url_https,true); 

			$args_twit_post = array(
		        'numberposts'      => $settings['count']*2,
		        'post_type'        => 'twit',
		        'meta_key' => 'twID',
		        'orderby' => 'meta_value_num',
	        ); 

		    $last_twit_post_arr = (wp_get_recent_posts( $args_twit_post));
			foreach ($last_twit_post_arr as $key => $twit_post) {
				if ($key > ($settings['count']-1)){ wp_delete_post($twit_post['ID']);}
			}
	    }     
  	} 		      
} 



