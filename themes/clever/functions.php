<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('clever', get_template_directory() . '/languages');

/**
 * Додаємо favico, charset, viewport
 */
function add_head_meta_tags()
{
    ?>
    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favico.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
}
add_action('wp_head', 'add_head_meta_tags');

/**
 * Реєструємо місце для меню
 */
add_action('after_setup_theme', function(){
  register_nav_menus( array(
      'primary' => __('Primary', 'clever'),
  ) );
});

/**
 * Реєструємо сайдбари теми
 */
if ( function_exists('register_sidebar') )
{
    register_sidebar(array(
        'id' => 'social',
        'name' => __('Шапка сайта', 'clever'),
        'description' => '',
        'before_widget' => '<div class="top-social-wrapper">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));
    register_sidebar( array(
        'id' => 'socialown',
        'name' => __('Страница контактов', 'clever'),
        'description' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
        $settings = get_option('clv');
        $wfoot = 'three';
        if ($settings['wfoot'] == 1) $wfoot = 'twelve';
        elseif ($settings['wfoot'] == 2) $wfoot = 'six';
        elseif ($settings['wfoot'] == 3) $wfoot = 'four';
        elseif ($settings['wfoot'] == 4) $wfoot = 'three';
    register_sidebar( array(
        'id' => 'footer',
        'name' => __('Footer сайта', 'clever'),
        'description' => '',
        'before_widget' => '<div class="footer-column ' . $wfoot. ' columns"><div class="widget widget_recent_entries gdlr-item gdlr-widget">',
        'after_widget' => '</div></div>',
        'before_title' => '<h3 class="gdlr-widget-title">',
        'after_title' => '</h3>'
    ));

    register_sidebar(array(
        'id' => 'center',
        'name' => __('Центральная секция страницы HOME', 'clever'),
        'description' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'id' => 'main',
        'name' => __('Контент страницы HOME', 'clever'),
        'description' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));    
}

// ****************************************************************

add_filter( 'the_content', 'create_link');

function create_link ($content){

  $content = ereg_replace("https://(([A-Za-z0-9.\-\/])*)", "<a href=\"\\0\" target=\"a_blank\">\\0</a>", $content);

  return $content;
}

// ************************************************************

function my_theme_add_editor_styles() {
  $font_url = 'https://fonts.google.com/?query=montser&selection.family=Montserrat:300,400,700';
  $font_url = str_replace( ',', '%2C', $font_url );
  add_editor_style( $font_url );
}
add_action( 'current_screen', 'my_theme_add_editor_styles' );

/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {

	add_theme_support( 'post-thumbnails' );  
}
/**
 * Можемо добавити різні розміри для картинок
 */
function mytheme_setup() {
  add_theme_support('custom-logo',array(
   'height' => 23,
   'width' => 200,
   'flex-height' => true,
   'flex-width' => true, 
    ) ); 
}

add_action('after_setup_theme', 'mytheme_setup');

if ( function_exists( 'add_image_size' )) { 
    add_image_size( 'small', 150, 150, array( 'center', 'top'));
    add_image_size( 'course', 700, 400, array( 'left', 'center'));
    add_image_size( 'blog', 400, 300, array( 'center', 'top'));    
}

/**
 * Реєструємо формати постів
 */
function add_post_formats(){
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'add_post_formats', 11 );

/**
 * Замінюємо стандартне закінчення обрізаного тексту з [...] на ...
 */
function custom_excerpt_more( $more ) {
	return ' ...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 10 );


/**
 * Підключаємо javascript файли
 */
function add_theme_scripts(){
    wp_enqueue_script("jquery");
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.js' );
    wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap') );
    wp_enqueue_script( 'core', get_template_directory_uri() . '/js/core.js' );
    wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js' );
    wp_enqueue_script( 'common', get_template_directory_uri() . '/js/common.js' );
    wp_enqueue_script( 'analytics', get_template_directory_uri() . '/js/analytics.js' );
    wp_enqueue_script( 'datepicker', get_template_directory_uri() . '/js/datepicker.js' );
    wp_enqueue_script( 'hoverIntent', get_template_directory_uri() . '/js/hoverIntent.js' );
    wp_enqueue_script( 'wp-emoji-release', get_template_directory_uri() . '/js/wp-emoji-release.js' );
    wp_enqueue_script( 'wp-embed', get_template_directory_uri() . '/js/wp-embed.js' );
    wp_enqueue_script( 'util', get_template_directory_uri() . '/js/util.js' );
    wp_enqueue_script( 'superfish', get_template_directory_uri() . '/js/superfish.js' );
    wp_enqueue_script( 'stats', get_template_directory_uri() . '/js/stats.js' );
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js' );
    wp_enqueue_script( 'maps', get_template_directory_uri() . '/js/maps.js' );
    wp_enqueue_script( 'lms-script', get_template_directory_uri() . '/js/lms-script.js' );
    wp_enqueue_script( 'npm', get_template_directory_uri() . '/js/npm.js' );
    wp_enqueue_script( 'jquery_002', get_template_directory_uri() . '/js/jquery_002.js' );
    wp_enqueue_script( 'jquery_003', get_template_directory_uri() . '/js/jquery_003.js' );
    wp_enqueue_script( 'jquery_004', get_template_directory_uri() . '/js/jquery_004.js' );
    wp_enqueue_script( 'jquery_005', get_template_directory_uri() . '/js/jquery_005.js' );
    wp_enqueue_script( 'jquery_006', get_template_directory_uri() . '/js/jquery_006.js' );
    wp_enqueue_script( 'jquery_007', get_template_directory_uri() . '/js/jquery_007.js' );
    wp_enqueue_script( 'jquery_008', get_template_directory_uri() . '/js/jquery_008.js' );
    wp_enqueue_script( 'jquery_009', get_template_directory_uri() . '/js/jquery_009.js' );
    wp_enqueue_script( 'gdlr-script', get_template_directory_uri() . '/js/gdlr-script.js' );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

/**
 * Підключаємо css файли
 */
function add_theme_style(){

    wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css' );
    wp_enqueue_style( 'lms-style', get_template_directory_uri() . '/css/lms-style.css');
    wp_enqueue_style( 'lms-style-custom', get_template_directory_uri() . '/css/lms-style-custom.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'superfish', get_template_directory_uri() . '/css/superfish.css');
    wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.css');
    wp_enqueue_style( 'style-responsive', get_template_directory_uri() . '/css/style-responsive.css');
    wp_enqueue_style( 'style-custom', get_template_directory_uri() . '/css/style-custom.css');
    wp_enqueue_style( 'styles', get_template_directory_uri() . '/css/styles.css'); 
    wp_enqueue_style( 'frontend', get_template_directory_uri() . '/css/frontend.css');
    wp_enqueue_style( 'masterslider', get_template_directory_uri() . '/css/masterslider.css');
    wp_enqueue_style( 'jquery-ui', get_template_directory_uri() . '/css/jquery-ui.css');
    wp_enqueue_style( 'jquery', get_template_directory_uri() . '/css/jquery.css');
    wp_enqueue_style( 'home-page', get_template_directory_uri() . '/css/home-page.css');
    wp_enqueue_style( 'custom', get_template_directory_uri() . '/css/custom.css');
    wp_enqueue_style( 'css_002', get_template_directory_uri() . '/css/css_002.css');
    wp_enqueue_style( 'css', get_template_directory_uri() . '/css/css.css');  
    wp_enqueue_style( 'component', get_template_directory_uri() . '/css/component.css');
    //wp_enqueue_style( 'tribe-events-full-mobile', get_template_directory_uri() . '/css/tribe-events-full-mobile.css');
    
    wp_enqueue_style( 'tribe-events-full', get_template_directory_uri() . '/css/tribe-events-full.css');
    wp_enqueue_style( 'tribe-events-theme', get_template_directory_uri() . '/css/tribe-events-theme.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );

/**
 * Пагінація
 */

// function wp_corenavi() {
//   global $wp_query;
//   $pages = '';
//   $max = $wp_query->max_num_pages;
//   if (!$current = get_query_var('paged')) $current = 1;
//   $a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
//   $a['total'] = $max;
//   $a['current'] = $current;

//   $total = 0; //1 - выводить текст "Страница N из N", 0 - не выводить
//   $a['mid_size'] = 1; //сколько ссылок показывать слева и справа от текущей
//   $a['end_size'] = 1; //сколько ссылок показывать в начале и в конце
//   $a['prev_text'] = 'Предыдущая страница'; //текст ссылки "Предыдущая страница"
//   $a['next_text'] = 'Следующая страница'; //текст ссылки "Следующая страница"

//   if ($max > 1) echo '<div class="navigation">';
//   if ($total == 1 && $max > 1) $pages = '<span class="pages">Страница ' . $current . ' из ' . $max . '</span>'."\r\n";
//   echo $pages . paginate_links($a);
//   if ($max > 1) echo '</div>';
// }


add_filter( 'nav_menu_css_class', 'filter_function_name_11' );
            function filter_function_name_11( $classes){
              
                $classes[] = "gdlr-normal-menu";
              
            return $classes;
          }


// ****************************************************************

class LastPostWidget extends WP_Widget
{
  public function __construct() {
      parent::__construct("TermShowWidget", "Последние посты",
          array("description" => "Виджет для показа последних постов"));
  }

  public function form($instance) {
    $title = "";
    $categoryId = ""; 
    $limit = NULL;
    $view = "";

    if (!empty($instance)) {
      $title = $instance["title"];
      $categoryId = $instance["categoryId"];
      $limit = $instance["limit"];
      $view = $instance["view"];
    }

    $titleId = $this->get_field_id("title");
    $titleName = $this->get_field_name("title");
    echo '<label for="' . $titleId . '">Заголовок:</label><br>'; 
    echo '<input  class="widefat" id="' . $titleId . '" type="text" name="' . $titleName . '" value="'. $title .'"><br>';
 
    $categoryId = $this->get_field_id("categoryId");
    $categoryName = $this->get_field_name("categoryId");
    echo '<label for="' . $categoryId . '">Выбор категории:</label><br>';
    echo '<select multiple="multiple" name="' . $categoryName .'[]" class="widefat" id="' . $categoryId . '">'?>
    <?php $categories = get_categories(); 
      foreach( $categories as $category ) {
          echo '<option value = "'. $category -> cat_ID.'"';
          if(!empty($instance['categoryId'])){
            foreach ($instance['categoryId'] as $value) {  
              if($value == $category -> cat_ID) echo 'selected="selected"';
            }
          } 
        echo' >'. $category -> name; echo '</option>'; 
      } 
    ?>
    <?php echo'</select><br><br>';

    $limitId = $this->get_field_id("limit");
    $limitName = $this->get_field_name("limit");
    echo '<label for="' . $limitId . '">Количество постов: <input class="tiny-text" id="' . $limitId . '" type="number" min="1" max="10" name="' . $limitName . '" value="' . $limit . '"></label><br><br>';

    $viewId = $this->get_field_id("view");
    $viewName = $this->get_field_name("view");
    echo '<p>Режим отображения:  </p>';
    echo '<label><input  id="' . $viewId . '" type="radio" name="' . $viewName . '" value="withImg"';if($instance["view"] == "withImg")echo 'checked="checked"';echo'> c картинками </label>';

    echo '<label><input  id="' . $viewId . '" type="radio" name="' . $viewName . '" value="onlyTitle"'; if($instance["view"] == "onlyTitle")echo 'checked="checked"';echo'> только заголовки </label><br><br>';
    return $categoryId;
  }

  public function update($newInstance, $oldInstance) {
    $values = array();
    $values["title"] = htmlentities($newInstance["title"]);
    $values["categoryId"] = $newInstance["categoryId"];
    $values["limit"] = htmlentities($newInstance["limit"]); 
    $values["view"] = $newInstance["view"]; 
    return $values;
  }

  public function widget($args, $instance) {
  ?>
    <?= $args['before_widget'] ?>
      <?= $args['before_title'] ?><?= $instance['title'];?><?= $args['after_title'];

      $cat=[];
      if(!empty($instance['categoryId'])){
          foreach ($instance['categoryId'] as $one) {           
            $cat[] = $one;         
          }   
      }

      $post_current = get_the_ID();
        $args_loop = array(
          'posts_per_page' => $instance['limit'],
          'cat' => $cat,
          'post__not_in'=> array ($post_current)
        );

    if($instance['view'] == 'withImg'){  
        
          $q = new WP_Query($args_loop);
            if($q->have_posts()) {
              while($q->have_posts()){ $q->the_post();?>

                <div class="gdlr-recent-post-widget">
                  <div class="recent-post-widget">
                    <div class="recent-post-widget-thumbnail">
                      <a href="<?=get_permalink();?>">
                        <?php
                        if (has_post_thumbnail()) {
                         the_post_thumbnail('small',
                         array(
                            'class' => 'img-responsive'
                              )
                            );   
                        }?>
                      </a>
                    </div>
                    <div class="recent-post-widget-content">
                      <div class="recent-post-widget-title">
                        <a href="<?=get_permalink();?>">
                          <?=get_the_title();?>
                        </a>
                      </div>
                      <div class="recent-post-widget-info">
                        <div class="blog-info blog-date">
                          <span class="gdlr-head">Posted on</span>
                          <a href=""><?php echo date('j M Y',strtotime(get_the_date()));?></a>
                        </div>
                        <div class="clear"></div>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div> 
                  <div class="clear"></div>
                </div>
        <?php }
            }
          wp_reset_postdata();?>     
        <?php       
    }else{ ?>
      <ul class="list-unstyled">
        <?php
          $q = new WP_Query($args_loop);
          if($q->have_posts()) {
            while($q->have_posts()){ $q->the_post();
            echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';  
            }
          }   
          wp_reset_postdata();?>     
      </ul> 



      <?php 
    }
    echo $args['after_widget'];
    return $instance['categoryId'];    
  }
}

add_action("widgets_init", function () {
    register_widget("LastPostWidget");
});

// *************************************************************

class LastNewsWidget extends WP_Widget
{
  public function __construct() {
      parent::__construct("LastPostWidget", "Новости.(для центральной секции)",
          array("description" => "Виджет для показа последних новостей"));
  }

  public function form($instance) {
    $title = "";
    $categoryId = ""; 
    $limit = NULL;
    $view = "";

    if (!empty($instance)) {
      $title = $instance["title"];
      $categoryId = $instance["categoryId"];
      $limit = $instance["limit"];
      $view = $instance["view"];
    }

    $titleId = $this->get_field_id("title");
    $titleName = $this->get_field_name("title");
    echo '<label for="' . $titleId . '">Заголовок:</label><br>'; 
    echo '<input  class="widefat" id="' . $titleId . '" type="text" name="' . $titleName . '" value="'. $title .'"><br>';
 
    $categoryId = $this->get_field_id("categoryId");
    $categoryName = $this->get_field_name("categoryId");
    echo '<label for="' . $categoryId . '">Выбор категории:</label><br>';
    echo '<select multiple="multiple" name="' . $categoryName .'[]" class="widefat" id="' . $categoryId . '">'?>
    <?php $categories = get_categories(); 
      foreach( $categories as $category ) {
          echo '<option value = "'. $category -> cat_ID.'"';
          if(!empty($instance['categoryId'])){
            foreach ($instance['categoryId'] as $value) {  
              if($value == $category -> cat_ID) echo 'selected="selected"';
            }
          } 
        echo' >'. $category -> name; echo '</option>'; 
      } 
    ?>
    <?php echo'</select><br><br>';

    $limitId = $this->get_field_id("limit");
    $limitName = $this->get_field_name("limit");
    echo '<label for="' . $limitId . '">Количество постов: <input class="tiny-text" id="' . $limitId . '" type="number" min="1" max="10" name="' . $limitName . '" value="' . $limit . '"></label><br><br>';

    $viewId = $this->get_field_id("view");
    $viewName = $this->get_field_name("view");
    echo '<p>Режим отображения:  </p>';
    echo '<label><input  id="' . $viewId . '" type="radio" name="' . $viewName . '" value="withImg"';if($instance["view"] == "withImg")echo 'checked="checked"';echo'> c картинками </label>';

    echo '<label><input  id="' . $viewId . '" type="radio" name="' . $viewName . '" value="onlyTitle"'; if($instance["view"] == "onlyTitle")echo 'checked="checked"';echo'> только заголовки </label><br><br>';
    return $categoryId;
  }

  public function update($newInstance, $oldInstance) {
    $values = array();
    $values["title"] = htmlentities($newInstance["title"]);
    $values["categoryId"] = $newInstance["categoryId"];
    $values["limit"] = htmlentities($newInstance["limit"]); 
    $values["view"] = $newInstance["view"]; 
    return $values;
  }

  public function widget($args, $instance) {
  ?>
  <?= $args['before_widget'] ?>
  <div class="four columns">
  <div class="gdlr-item-title-wrapper gdlr-item pos-left ">
    <div class="gdlr-item-title-head" style="padding: 0 15px">
      <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border"><?=$instance['title']?></h3>
      <div class="clear"></div>
    </div>
    <a class="gdlr-item-title-link" href="<?=get_permalink().'blog__trashed/blog-3-columns/';?>">Read The Blog</a>
  </div>

  <div class="blog-item-wrapper" style="margin: 0 15px;">
    <div class="blog-item-holder">
      <div class="clear"></div>
      
<?php 
      $cat=[];
      if(!empty($instance['categoryId'])){
          foreach ($instance['categoryId'] as $one) {           
            $cat[] = $one;         
          }   
      }
      $post_current = get_the_ID();
        $args_loop = array(
          'posts_per_page' => $instance['limit'],
          'cat' => $cat,
          'post__not_in'=> array ($post_current)
        );
      if($instance['view'] == 'withImg'){  
          $q = new WP_Query($args_loop);
            if($q->have_posts()) {
              while($q->have_posts()){ $q->the_post();  ?>

                <div class="twelve columns">         
                  <div class="gdlr-item gdlr-blog-widget">
                    <div class="gdlr-ux gdlr-blog-widget-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                      <div class="gdlr-standard-style">



                        <div class="gdlr-blog-thumbnail">
                          <a href="<?=get_permalink();?>">
                            <?php
                            if (has_post_thumbnail()) {
                             the_post_thumbnail('small',
                               array(
                                'class' => 'img-responsive'
                                )
                               );   
                             }?>
                           </a>    
                         </div>
                         <header class="post-header">
                          <h3 class="gdlr-blog-title">
                            <a href="<?=get_permalink();?>">
                              <?=get_the_title();?>
                            </a>
                          </h3>
                          <div class="gdlr-blog-info gdlr-info">
                            <div class="blog-info blog-date">
                              <span class="gdlr-head">Posted on</span>
                              <a href=""><?php echo date('j M Y',strtotime(get_the_date()));?></a>
                            </div>
                            <div class="clear"></div>
                          </div>    
                          <div class="clear"></div>
                        </header>
                        <div class="clear"></div>
                      </div> 
                    </div>
                  </div>
                </div>                  
        <?php }
            }
          wp_reset_postdata();?>       
      <?php    
      } else { ?>
      <div class="twelve columns">         
        <div class="gdlr-item gdlr-blog-widget">
          <div class="gdlr-ux gdlr-blog-widget-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
            <div class="gdlr-standard-style">
              <ul class="list-unstyled">
                <?php
                $q = new WP_Query($args_loop);
                if($q->have_posts()) {
                  while($q->have_posts()){ $q->the_post();
                    echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';  
                  }
                }   
                wp_reset_postdata();?>     
              </ul>
            </div>  
          </div>
        </div>
      </div>
  <?php
      } ?>
    </div> 
    <div class="clear"></div>
  </div> 
  </div><?php
  echo $args['after_widget'];
  return $instance['categoryId'];    
  }
}

add_action("widgets_init", function () {
    register_widget("LastNewsWidget");
});

// ******************************************************************

add_action( 'init', 'register_post_type_instructors' );

function register_post_type_instructors() {
 $labels = array(
     'name' => 'Инструкторы',
      'add_new' => 'Добавить нового',
      'all_items' => 'Все инструкторы',
   );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'show_ui' => true, // показывать интерфейс в админке
    'has_archive' => true, 
    //'menu_icon' => get_stylesheet_directory_uri() .'/img/function_icon.png', // иконка в меню
    //'menu_icon' => '', // иконка в меню
    'menu_position' => 5, // порядок в меню
    'supports' => array( 'title', 'editor', 'comments', 'author', 'thumbnail','excerpt')
  );
  register_post_type('instructors', $args);
}

add_action( 'init', 'register_post_type_courses' );

function register_post_type_courses() {
 $labels = array(
     'name' => 'Курсы',
      'add_new' => 'Добавить новый',
      'all_items' => 'Все курсы',
   );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'show_ui' => true,
    'has_archive' => true, 
    'menu_position' => 5, 
    'supports' => array( 'title', 'editor', 'comments', 'author', 'thumbnail','excerpt')
  );
  register_post_type('courses', $args);
}

add_action( 'init', 'register_post_type_services' );

function register_post_type_services() {
 $labels = array(
     'name' => 'Сервисы',
      'add_new' => 'Добавить новый',
      'all_items' => 'Все сервисы',
   );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'show_ui' => true,
    'has_archive' => true, 
    'menu_position' => 9,
    'supports' => array( 'title', 'editor', 'comments', 'author', 'thumbnail','excerpt')
  );
  register_post_type('services', $args);
}

// add_action( 'init', 'register_post_type_events' );

// function register_post_type_events() {
//  $labels = array(
//      'name' => 'События',
//       'add_new' => 'Добавить новое',
//       'all_items' => 'Все события',
//    );
//   $args = array(
//     'labels' => $labels,
//     'public' => true,
//     'show_ui' => true, 
//     'has_archive' => true, 
//     'menu_position' => 5, 
//     'supports' => array( 'title', 'editor', 'comments', 'author', 'thumbnail', 'excerpt')
//   );
//   register_post_type('events', $args);
// }

// ***********************************************************************

class CentralSection extends WP_Widget
{
  public function __construct() {
      parent::__construct("LastNewsWidget", "6.Центральная секция",
          array("description" => "Виджет для центральной секции."));
  }

  public function widget($args, $instance) {
  ?>
    <?= $args['before_widget'] ?>
    <section id="content-section-6">
      <div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #ffffff; padding-top: 90px; padding-bottom: 30px; ">
        <div class="container">

          <?php dynamic_sidebar('center'); ?>   

          <div class="clear"></div>
        </div>
      </div>
      <div class="clear"></div>
    </section>
    <?php echo $args['after_widget'];
  }
}

add_action("widgets_init", function () {
    register_widget("CentralSection");
});

// ******************************************************************************

class Section3_Lern_more extends WP_Widget
{
  public function __construct() {
      parent::__construct("Section2_Services", "3.Секция Ссылка",
          array("description" => "Виджет для показа ссылки"));
  }

  public function form($instance) {
    $title = "";
    $text = "";
    $button_text = "";
    $link = "";

    if (!empty($instance)) {
      $title = $instance["title"];
      $text = $instance["text"];
      $link = $instance["link"];
    }

    $titleId = $this->get_field_id("title");
    $titleName = $this->get_field_name("title");
    echo '<label for="' . $titleId . '">Заголовок:</label><br>'; 
    echo '<input  class="widefat" id="' . $titleId . '" type="text" name="' . $titleName . '" value="'. $title .'"><br>';

    $textId = $this->get_field_id("text");
    $textName = $this->get_field_name("text");
    echo '<label for="' . $textId . '">Основной текст:</label><br>'; 
    echo '<textarea class="widefat" id="' . $textId . '" name="' . $textName . '">'. $text .'</textarea><br><br>';

     $linkId = $this->get_field_id("link");
    $linkName = $this->get_field_name("link");
    echo '<label for="' . $linkId . '">Ссылка:</label><br>';
    echo '<input  class="widefat" id="' . $linkId . '" type="text" name="' . $linkName . '" value="'. $link .'"><br>';   
  }

  public function update($newInstance, $oldInstance) {
    $values = array();
    $values["title"] = htmlentities($newInstance["title"]); 
    $values["text"] = htmlentities($newInstance["text"]); 
    $values["link"] = htmlentities($newInstance["link"]); 
    return $values;
  }

  public function widget($args, $instance) {?>
  <?= $args['before_widget'] ?>
  
  <section id="content-section-3">
    <div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #f5f5f5; padding-top: 40px; padding-bottom: 20px; ">
      <div class="container">
        <div class="gdlr-stunning-text-ux gdlr-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
          <div class="gdlr-item gdlr-stunning-text-item gdlr-button-on type-normal" style=" margin: 0 15px 20px;">
            <h2 class="stunning-text-title"><?=$instance['title'];?></h2>
            <div class="stunning-text-caption gdlr-skin-content">
              <p><?=$instance['text'];?></p>
            </div>
            <a class="stunning-text-button gdlr-button" href="<?=get_bloginfo('template_url');?><?=$instance['link'];?>" target="_blank">Learn more</a>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
  </section>
<?php              
echo $args['after_widget'];   
}
}

add_action("widgets_init", function () {
    register_widget("Section3_Lern_more");
});

// ****************************************************

class Video extends WP_Widget
{
  public function __construct() {
      parent::__construct("Section3_Lern_more", "Видео. (для центральной секции)",
          array("description" => "Виджет для показа видео"));
  }

  public function form($instance) {
    $title = "";
    $link = "";

    if (!empty($instance)) {
      $title = $instance["title"];
      $titleH6 = $instance["titleH6"];  
      $link = $instance["link"];
      $text = $instance["text"];
    }

    $titleId = $this->get_field_id("title");
    $titleName = $this->get_field_name("title");
    echo '<label for="' . $titleId . '">Заголовок:</label><br>'; 
    echo '<input  class="widefat" id="' . $titleId . '" type="text" name="' . $titleName . '" value="'. $title .'"><br>';

    $titleH6Id = $this->get_field_id("titleH6");
    $titleH6Name = $this->get_field_name("titleH6");
    echo '<label for="' . $titleH6Id . '">Заголовок текста:</label><br>'; 
    echo '<input  class="widefat" id="' . $titleH6Id . '" type="text" name="' . $titleH6Name . '" value="'. $titleH6 .'"><br>';

    $linkId = $this->get_field_id("link");
    $linkName = $this->get_field_name("link");
    echo '<label for="' . $linkId . '">Ссылка:</label><br>';
    echo '<input  class="widefat" id="' . $linkId . '" type="text" name="' . $linkName . '" value="'. $link .'"><br><br>';

    $textId = $this->get_field_id("text");
    $textName = $this->get_field_name("text");
    echo '<label for="' . $textId . '">Основной текст:</label><br>'; 
    echo '<textarea class="widefat" id="' . $textId . '" name="' . $textName . '">'. $text .'</textarea><br><br>';  
  }

  public function update($newInstance, $oldInstance) {
    $values = array();
    $values["title"] = htmlentities($newInstance["title"]); 
    $values["titleH6"] = htmlentities($newInstance["titleH6"]); 
    $values["link"] = htmlentities($newInstance["link"]); 
    $values["text"] = htmlentities($newInstance["text"]); 
    return $values;
  }

  public function widget($args, $instance) {?>
  <?= $args['before_widget'] ?>
  <div class="four columns">
        <div class="gdlr-title-item" style="margin-bottom: 40px;">
          <div class="gdlr-item-title-wrapper gdlr-item pos-left ">
            <div class="gdlr-item-title-head" style="padding: 0 15px">
              <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border"><?=$instance["title"]?></h3>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="gdlr-item gdlr-content-item" style="margin: 0 15px">
          <p>
            <div class="gdlr-fluid-video-wrapper" style="padding-top: 56%;">
              <iframe src="<?=$instance["link"]?>"  frameborder="0" allowfullscreen></iframe>
            </div>
          </p>
          <div class="clear" style="margin-top: 25px;">&nbsp;</div>
          <h6><?=$instance["titleH6"]?></h6>
          <p><?=$instance["text"]?></p>
        </div>
      </div>
   <?php echo $args['after_widget'];   
}
}

add_action("widgets_init", function () {
    register_widget("Video");
});

// *****************************************************

class FeaturedInstructor extends WP_Widget
{
  public function __construct() {
      parent::__construct("Video", "Популярный преподаватель.(для центр.секции)",
          array("description" => "Виджет для показа преподавателя."));
  }
  public function form($instance) {

    $title = "";
    $post = ""; 

    if (!empty($instance)) {
      $title = $instance["title"];
      $post = $instance["post"];
    }

    $titleId = $this->get_field_id("title");
    $titleName = $this->get_field_name("title");
    echo '<label for="' . $titleId . '">Заголовок:</label><br>'; 
    echo '<input  class="widefat" id="' . $titleId . '" type="text" name="' . $titleName . '" value="'. $title .'"><br>';

    $postId = $this->get_field_id("post");
    $postName = $this->get_field_name("post");
    echo '<label for="' . $postId . '">Выбор поста:</label><br>';
    echo '<select name="' . $postName .'" class="widefat" id="' . $postId . '">';

    $args = array(
              'post_type' => 'instructors',
              );
    $postarray  = get_posts($args);

        foreach( $postarray as $post ) {
          echo '<option value = "'. $post -> ID .'"';
          if(!empty($instance['post'])){   
              if ($instance['post'] == $post -> ID) echo 'selected="selected"'; 
          } 
          echo' >'. $post -> post_title .'</option>'; 
        }   
    ?>
    <?php echo'</select><br><br>';
    return $post-> ID;
  }

  public function update($newInstance, $oldInstance) {
    $values = array();
    $values["title"] = htmlentities($newInstance["title"]);
    $values["post"] = htmlentities($newInstance["post"]);
    
    return $values;
  }

  public function widget($args, $instance) {
  ?>
    <?= $args['before_widget'] ?>
        <div class="four columns">
              
        <div class="gdlr-item-title-wrapper gdlr-item pos-left ">
          <div class="gdlr-item-title-head" style="padding: 0 15px">
            <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border"><?=$instance['title']?></h3>
            <div class="clear"></div>
          </div>
        </div>
        <div class="instructor-item-wrapper">
          <div class="gdlr-lms-instructor-grid-wrapper">
            <div class="clear"></div>
            <?php
            $args_loop = array(
              'post_type' => 'instructors',
              'p' => $instance['post'],
              );
            $q = new WP_Query($args_loop);
            if($q->have_posts()) {
              while($q->have_posts()){ $q->the_post();
                ?>
                <div class="gdlr-lms-instructor-grid gdlr-lms-col1">
                  <div class="gdlr-lms-item">
                    <div class="gdlr-lms-instructor-content">
                      <div class="gdlr-lms-instructor-thumbnail">
                        <?php
                        if (has_post_thumbnail()) {
                          the_post_thumbnail('small',
                            array(
                              'class' => 'img-responsive pull-left'
                              )
                            ); 
                          }?>
                      </div>
                      <div class="gdlr-lms-instructor-title-wrapper">
                        <h3 class="gdlr-lms-instructor-title">
                          <?=get_the_title();?>
                        </h3>
                        <div class="gdlr-lms-instructor-position"><?=get_post_meta($q->post->ID, "instructor-position",true);?></div>
                      </div>
                      <div class="gdlr-lms-author-description"><?=wp_trim_words(get_post_meta($q->post->ID, "instructor-description",true),'10');?></div>
                      <a class="gdlr-lms-button cyan" href="<?=get_permalink();?>">View Profile</a>
                    </div>
                    <div class="clear"></div>
                  </div>
                </div>
          <?php  }
            } 
            wp_reset_postdata();?> 

                <div class="clear"></div>
          </div>
        </div>
      </div> 
    <?php
    echo $args['after_widget'];  
  }
}

add_action("widgets_init", function () {
    register_widget("FeaturedInstructor");
});

// ****************************************************************


class Section2_Services extends WP_Widget
{
  public function __construct() {
      parent::__construct("CentralSection", "2.Секция Сервисы",
          array("description" => "Виджет для показа сервисов"));
  }

  public function form($instance) {
    $limit = NULL;


    if (!empty($instance)) {
      $limit = $instance["limit"];
    }

    $limitId = $this->get_field_id("limit");
    $limitName = $this->get_field_name("limit");
    echo '<label for="' . $limitId . '">Количество сервисов на странице: <input class="tiny-text" id="' . $limitId . '" type="number" min="1" max="4" name="' . $limitName . '" value="' . $limit . '"></label><br><br>';
  }

  public function update($newInstance, $oldInstance) {
    $values = array();
    $values["limit"] = htmlentities($newInstance["limit"]); 
    return $values;
  }

  public function widget($args, $instance) {?>
  <?= $args['before_widget'] ?>
      <?= $args['before_title'] ?><?= $instance['title'];?><?= $args['after_title'];?>

      <section id="content-section-2">
        <div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #ffffff; padding-top: 75px; padding-bottom: 45px; ">
          <div class="container">

            <?php
            $args_loop = array(
              'post_type' => 'services',
              'posts_per_page' => $instance['limit'] 
              );
            $q = new WP_Query($args_loop);
            if($q->have_posts()) {
             while($q->have_posts()){ $q->the_post();?>
              <div class="<?php 
                if ($instance['limit'] == 1) echo 'twelve';
                elseif ($instance['limit'] == 2) echo 'six';
                elseif ($instance['limit'] == 3) echo 'four';
                elseif ($instance['limit'] == 4) echo 'three';
             ?> columns">
                <div class="gdlr-ux column-service-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
                  <div class="gdlr-item gdlr-column-service-item gdlr-type-2" style="margin: 0 15px 20px;">
                    <div class="column-service-image">
                      <?=the_post_thumbnail();?>
                    </div>
                    <div class="column-service-content-wrapper">
                      <h3 class="column-service-title"><?=get_the_title();?></h3>
                      <div class="column-service-content gdlr-skin-content">
                        <p><?=the_content();?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php }
          }
          wp_reset_postdata();?>      
          <div class="clear"></div>
        </div>
      </div>
      <div class="clear"></div>
    </section> 
    <?php              
  echo $args['after_widget'];   
  }
}

add_action("widgets_init", function () {
    register_widget("Section2_Services");
});

// ****************************************************************************

add_action( 'admin_menu', function(){
  add_menu_page( 'Настройки темы', 'Настройки темы CleverCourse', 8 , __FILE__ , 'show_theme_option' );
});

function show_theme_option(){

  $fields = [
      [
          'label' => 'адрес (так будет отображен на сайте)',
          'name' => 'addres',
          'value' => null,
          'type' => 'text'
      ],
      [
          'label' => 'страна',
          'name' => 'country',
          'value' => null,
          'type' => 'text'
      ],
      [
          'label' => 'город',
          'name' => 'city',
          'value' => null,
          'type' => 'text'
      ],
      [
          'label' => 'улица и номер дома',
          'name' => 'street',
          'value' => null,
          'type' => 'text'
      ],
      [
          'label' => 'телефон',
          'name' => 'phone',
          'value' => null,
          'type' => 'text'
      ],
      [
          'label' => 'электронная почта',
          'name' => 'email',
          'value' => null,
          'type' => 'text'
      ],[
          'label' => 'расписание',
          'name' => 'schedule',
          'value' => null,
          'type' => 'textarea'
      ],
      [
          'label' => 'вступительный текст на странице "Контакты"',
          'name' => 'text1',
          'value' => null,
          'type' => 'textarea'
      ], 
      [
          'label' => 'количество виджетов в ряду футера',
          'name' => 'wfoot',
          'value' => null,
          'type' => 'number'
      ],
  ];

  $settings = [];
  if(isset($_POST['hidden'])){
    foreach ($_POST as $field => $value){
        $settings[ $field ] = $value;
    }
    update_option('clv', $settings);
  }
  $settings = get_option('clv'); 

  ?>
  <div class="wrap">
  <h1>Настройки темы CleverCourse</h1>
  <form name="clevercourse" method="POST">
    <table class="form-table">
      <tbody>
    <?php foreach ($fields as $field){
          $field['value'] = $settings[ $field['name']] ;
          ?>
          <tr>
              <th scope="row"><label><?= $field['label'] ?></label></th>
              <td>
                <?php 
                if ($field['type'] == 'textarea'){?>
                 <textarea rows="4" cols="30" name="<?=$field['name'];?>"><?=$field['value'];?></textarea> 
          <?php } else { ?>
                <input type="<?= $field['type'] ?>" name="<?=$field['name'] ?>" <?php if ( $field['type'] == 'number') echo 'min="1" max="4"'?> value="<?=$field['value'] ?>" >
          <?php } ?>
              </td>
          </tr> 
    <?php }?>
      </tbody>
    </table>
    <input type="hidden" name="hidden">
    <input type="submit" class="button button-primary button-large" value="Обновить настройки">
    <?php wp_nonce_field(); ?>
  </form>
  </div>      
<?php
}

// *********************************************************************

class Section4_Recent_Courses extends WP_Widget
{
  public function __construct() {
      parent::__construct("FeaturedInstructor", "4.Секция Курсы",
          array("description" => "Виджет для показа курсов"));
  }

  public function form($instance) {
    $limit = NULL;
    $title = '';

    if (!empty($instance)) {
      $title = $instance["title"];
      $limit = $instance["limit"];
    }

    $titleId = $this->get_field_id("title");
    $titleName = $this->get_field_name("title");
    echo '<label for="' . $titleId . '">Заголовок:</label><br>'; 
    echo '<input  class="widefat" id="' . $titleId . '" type="text" name="' . $titleName . '" value="'. $title .'"><br>';

    $limitId = $this->get_field_id("limit");
    $limitName = $this->get_field_name("limit");
    echo '<label for="' . $limitId . '">Количество курсов на странице: <input class="tiny-text" id="' . $limitId . '" type="number" min="1" max="4" name="' . $limitName . '" value="' . $limit . '"></label><br><br>';
  }

  public function update($newInstance, $oldInstance) {
    $values = array();
    $values["title"] = htmlentities($newInstance["title"]);
    $values["limit"] = htmlentities($newInstance["limit"]);    
    return $values;
  }

  public function widget($args, $instance) {?>
  <?= $args['before_widget'] ?>
      <section id="content-section-4">
        <div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #ffffff; padding-top: 90px; padding-bottom: 60px; ">
          <div class="container">
            <div class="gdlr-item-title-wrapper gdlr-item pos-center gdlr-nav-container ">
              <div class="gdlr-item-title-head"><i class="fa fa-angle-left icon-angle-left gdlr-flex-prev"></i>
              <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border"><?= $instance['title'];?></h3>
                <i class="fa fa-angle-right icon-angle-right gdlr-flex-next"></i>
                <div class="clear"></div>
              </div>
              <a class="gdlr-item-title-link" href="<?=get_template_directory_uri();?>category/courses/">View All Courses</a>
            </div>
            <div class="course-item-wrapper">
              <div class="gdlr-lms-course-grid2-wrapper gdlr-lms-carousel">
                <div class="flexslider" data-type="carousel" data-nav-container="course-item-wrapper" data-columns="<?=$instance['limit'];?>">
                  <div class="flex-viewport" style="overflow: hidden; position: relative;">
                    <ul class="slides" style="width: 1400%; margin-left: 0px;">

                      <?php
                      $args_loop = array(
                        'post_type' => 'courses' 
                        );
                      $q = new WP_Query($args_loop);
                      if($q->have_posts()) {
                        while($q->have_posts()){ $q->the_post();?>

                        <li class="gdlr-lms-course-grid2 gdlr-lms-item gdlr-lms-free" style="width: 360px; float: left; display: block; height: 382px;">
                          <div class="gdlr-lms-course-thumbnail">
                            <a href="<?=get_permalink();?>">
                              <?php 
                              the_post_thumbnail('course',
                                array(
                                  'class' => 'img-responsive'
                                  )
                                  );?>
                                </a>
                              </div>
                              <div class="gdlr-lms-course-content">
                                <h3 class="gdlr-lms-course-title">
                                  <a href="<?=get_permalink();?>"><?=get_the_title();?></a>
                                </h3>
                                <div class="gdlr-lms-course-price">
                                  <span class="price-button blue">Free</span>
                                </div>
                                <div class="gdlr-lms-course-info">
                                  <i class="fa fa-clock-o icon-time"></i>
                                  <span class="tail">Mar 06, 2017 - May 21, 2017</span>
                                </div>
                                <div class="clear"></div>
                              </div>
                            </li>

                            <?php }
                          }
                          wp_reset_postdata();?> 
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
            </div>
            <div class="clear"></div>
          </section>
    <?php              
  echo $args['after_widget'];   
  }
}

add_action("widgets_init", function () {
    register_widget("Section4_Recent_Courses");
});

// ****************************************************************

class Section5_Search extends WP_Widget
{
  public function __construct() {
      parent::__construct("Section4_Recent_Courses", "5.Секция Поиск курсов",
          array("description" => "Виджет для поиска курсов"));
  }

  public function widget($args, $instance) {?>
  <?= $args['before_widget'] ?>
      <section id="content-section-5">
  <div class="gdlr-parallax-wrapper gdlr-background-image gdlr-show-all gdlr-skin-dark-skin" id="gdlr-parallax-wrapper-1" data-bgspeed="0" style="background-image: url('<?php bloginfo('template_url');?>/images/search-bg.jpg'); padding-top: 130px; padding-bottom: 90px; ">

    <div class="container">
      <div class="gdlr-item-title-wrapper gdlr-item pos-center ">
        <div class="gdlr-item-title-head">
          <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border">Search For Courses</h3>
          <div class="clear"></div>
        </div>
        <div class="gdlr-item-title-divider">

        </div>
        <div class="gdlr-item-title-caption gdlr-skin-info">Fill keywords to seek for courses</div>
      </div>
      <div class="course-search-wrapper">
        <form method="get" id="searchform" class="gdlr-lms-form" action="<?php echo esc_url(home_url('/')); ?>">
          <div class="course-search-column gdlr-lms-1">
            <span class="gdlr-lms-combobox">
              <select name="course_category">

                <option value="" selected="selected" >Category</option>
                $categories_key = [];
                $categories_value = [];
                $types_key = [];
                $types_value = [];

              <?php foreach(get_posts(array('post_type' => 'courses','numberposts' => 50)) as $post): 

                  $course_category = get_the_terms($post->ID, 'course_category')[0];
                  $course_type = get_the_terms($post->ID, 'course_type')[0];

                  $categories_key[] = $course_category -> slug;
                  $categories_value[] = $course_category -> name;

                  $types_key[] = $course_type -> slug;
                  $types_value[] = $course_type -> name;

                  endforeach ;

                  $categories = array_combine($categories_key , $categories_value);
                  $types = array_combine($types_key , $types_value);

                  foreach($categories as $key => $category):?>
                  <option value="<?=$key;?>"><?=$category;?></option>
            <?php endforeach ;?>

              </select>
            </span>

          </div>
          <div class="course-search-column gdlr-lms-2">
            <span class="gdlr-lms-combobox">
              <select name="course_type" id="gender">
                <option value="" selected="selected" >Type</option>
              <?php foreach($types as $key => $type):?>
                  <option value="<?=$key?>"><?=$type?></option>
              <?php endforeach ;?>
              </select>
            </span> 
          </div>
          <div class="course-search-column gdlr-lms-3">
            <input name="s" id="s" autocomplete="off" placeholder="Keywords" type="text">
          </div>
          <div class="course-search-column gdlr-lms-4">
            <input name="post_type" value="courses" type="hidden">
            <input class="gdlr-lms-button" value="Search!" type="submit">
          </div>
          <div class="clear"></div>

        </form>

      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="clear"></div>
</section>
    <?php              
  echo $args['after_widget'];   
  }
}

add_action("widgets_init", function () {
    register_widget("Section5_Search");
});

// ***********************************************************

class Section7_Student_say extends WP_Widget

{
  public function __construct() {
      parent::__construct("Section5_Search", "7.Секция отзывы студентов",
          array("description" => "Виджет для показа отзывов"));
  }

  public function form($instance) {
    $limit = NULL;
    $title = '';

    if (!empty($instance)) {
      $title = $instance["title"];
      $limit = $instance["limit"];
    }

    $titleId = $this->get_field_id("title");
    $titleName = $this->get_field_name("title");
    echo '<label for="' . $titleId . '">Заголовок:</label><br>'; 
    echo '<input  class="widefat" id="' . $titleId . '" type="text" name="' . $titleName . '" value="'. $title .'"><br>';

    $limitId = $this->get_field_id("limit");
    $limitName = $this->get_field_name("limit");
    echo '<label for="' . $limitId . '">Количество курсов на странице: <input class="tiny-text" id="' . $limitId . '" type="number" min="1" max="4" name="' . $limitName . '" value="' . $limit . '"></label><br><br>';
  }

  public function update($newInstance, $oldInstance) {
    $values = array();
    $values["title"] = htmlentities($newInstance["title"]);
    $values["limit"] = htmlentities($newInstance["limit"]);    
    return $values;
  }

  public function widget($args, $instance) {?>
  <?= $args['before_widget'] ?>
  <section id="content-section-7">
    <div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #f5f5f5; padding-top: 85px; padding-bottom: 45px; "><div class="container">
      <div class="gdlr-testimonial-item-wrapper">
        <div class="gdlr-item-title-wrapper gdlr-item pos-center gdlr-nav-container ">
          <div class="gdlr-item-title-head">
            <i class="fa fa-angle-left icon-angle-left gdlr-flex-prev"></i>
            <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border"><?=$instance["title"]?></h3>
            <i class="fa fa-angle-right icon-angle-right gdlr-flex-next"></i>
            <div class="clear"></div>
          </div>
          <div class="gdlr-item-title-divider"></div>
        </div>
        <div class="gdlr-item gdlr-testimonial-item carousel plain-style">
          <div class="gdlr-ux gdlr-testimonial-ux" style="opacity: 1; padding-top: 0px; margin-bottom: 0px;">
            <div class="flexslider" data-type="carousel" data-nav-container="gdlr-testimonial-item" data-columns="1">
              <div class="flex-viewport" style="overflow: hidden; position: relative;">
                <ul class="slides" style="width: 600%; margin-left: -2280px;">
                  
                     <?php
                      if( have_rows('exerpt') ): 
                        while ( have_rows('exerpt') ) : the_row();?>
                          <li class="testimonial-item" style="width: 1110px; float: left; display: block;">
                            <div class="testimonial-content-wrapper" style="background-image:url('<?php bloginfo("template_url");?>/images/testimonial-quote.png') no-repeat">
                              <div class="testimonial-content gdlr-skin-content">
                                <p><?php the_sub_field('exerpt1'); ?></p>
                              </div>
                              <div class="testimonial-info">
                                <span class="testimonial-author gdlr-skin-link-color"><?php the_sub_field('name'); ?></span>
                                <span class="testimonial-position gdlr-skin-info"><span>, </span><?php the_sub_field('profession'); ?></span>
                              </div>
                            </div>
                          </li>     
                    <?php endwhile;
                      endif; 
                      ?>

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div class="clear"></div>
</section>
    <?php              
  echo $args['after_widget'];   
  }
}

add_action("widgets_init", function () {
    register_widget("Section7_Student_say");
});

// *********************************************************************

class Section8_Popular_courses extends WP_Widget
{
  public function __construct() {
      parent::__construct("Section7_Student_say", "8.Популярные курсы",
          array("description" => "Виджет для показа отзывов"));
  }

  public function form($instance) {
    $limit = NULL;
    $title = '';
    $subtitle = '';

    if (!empty($instance)) {
      $title = $instance["title"];
      $subtitle = $instance["subtitle"];
      $limit = $instance["limit"];
    }

    $titleId = $this->get_field_id("title");
    $titleName = $this->get_field_name("title");
    echo '<label for="' . $titleId . '">Заголовок:</label><br>'; 
    echo '<input  class="widefat" id="' . $titleId . '" type="text" name="' . $titleName . '" value="'. $title .'"><br>';

    $subtitleId = $this->get_field_id("subtitle");
    $subtitleName = $this->get_field_name("subtitle");
    echo '<label for="' . $subtitleId . '">Подзаголовок:</label><br>'; 
    echo '<input  class="widefat" id="' . $subtitleId . '" type="text" name="' . $subtitleName . '" value="'. $subtitle .'"><br>';

    $limitId = $this->get_field_id("limit");
    $limitName = $this->get_field_name("limit");
    echo '<label for="' . $limitId . '">Количество курсов на странице: <input class="tiny-text" id="' . $limitId . '" type="number" min="1" max="5" name="' . $limitName . '" value="' . $limit . '"></label><br><br>';
  }

  public function update($newInstance, $oldInstance) {
    $values = array();
    $values["title"] = htmlentities($newInstance["title"]);
    $values["subtitle"] = htmlentities($newInstance["title"]);
    $values["limit"] = htmlentities($newInstance["limit"]);    
    return $values;
  }

  public function widget($args, $instance) {?>
  <?= $args['before_widget'] ?>
  <section id="content-section-8">
    <div class="gdlr-color-wrapper  gdlr-show-all no-skin" style="background-color: #ffffff; padding-top: 85px; padding-bottom: 60px; ">
      <div class="container">

        <div class="gdlr-title-item" style="margin-bottom: 50px;">
          <div class="gdlr-item-title-wrapper gdlr-item pos-center ">
            <div class="gdlr-item-title-head">
            <h3 class="gdlr-item-title gdlr-skin-title gdlr-skin-border"><?=$instance["title"]?></h3>
              <div class="clear"></div>
            </div>
            <div class="gdlr-item-title-divider"></div>
            <div class="gdlr-item-title-caption gdlr-skin-info"><?=$instance["subtitle"]?></div>
          </div>
        </div>
        <div class="gdlr-banner-item-wrapper">
          <div class="gdlr-banner-images gdlr-item" style="margin-bottom: 0px;">
            <div class="flexslider" data-pausetime="7000" data-slidespeed="600" data-effect="fade" data-columns="<?=$instance["limit"]?>" data-type="carousel" data-nav-container="gdlr-banner-images">
              
                <ul class="slides" style="width: 600%; margin-left: 0px;">

                  <?php
                  $args_loop = array( 
                    
                    'post_type' => 'courses',
                    'order' => 'DESC',
                    'posts_per_page' => 3, 
                    // 'meta_key' => 'baner',
                    // 'meta_value' => '',
                    // 'meta_compare' => '!='
                    
                    );
                  $q = new WP_Query($args_loop);
                  if($q->have_posts()) {
                    while($q->have_posts()){ $q->the_post();?>
                      <li style="width: 350px; float: left; display: block;">
                        <a href="<?=the_permalink();?>" data-rel="fancybox" target="_blank"> 
                          <img src="<?php the_field('baner', $q->post->ID);?>">
                        </a>
                      </li>
                      <?php               
                    }
                  }
                  wp_reset_postdata();?> 
                </ul>
              </div>
              <!-- <ul class="flex-direction-nav">
                <li>
                  <a class="flex-prev flex-disabled" href="#" tabindex="-1">
                    <i class="icon-angle-left"></i>
                  </a>
                </li>
                <li>
                  <a class="flex-next flex-disabled" href="#" tabindex="-1">
                    <i class="icon-angle-right"></i>
                  </a>
                </li>
              </ul> -->
            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
  </section>
  <?php              
  echo $args['after_widget'];   
  }
}

add_action("widgets_init", function () {
    register_widget("Section8_Popular_courses");
});




