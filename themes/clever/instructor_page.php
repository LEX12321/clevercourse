<?php
/*
Template Name: Instructor page 
*/
?>
<?php get_header(); ?>
<div class="content-wrapper">
	<div class="gdlr-content">
		<div class="with-sidebar-wrapper">
			<section id="content-section-1">
				<div class="section-container container">
					<div class="instructor-item-wrapper" style="margin-bottom: 30px;">
						<div class="gdlr-lms-instructor-grid-wrapper">
							<div class="clear"></div>
							<?php
							$args_loop = array(
					        'post_type' => 'instructors' 
					        );
					        $q = new WP_Query($args_loop);
					            if($q->have_posts()) {
					              	while($q->have_posts()){ $q->the_post();
					        ?>
									<div class="gdlr-lms-instructor-grid gdlr-lms-col3">
										<div class="gdlr-lms-item">
											<div class="gdlr-lms-instructor-content">
												<div class="gdlr-lms-instructor-thumbnail">
													<?php
								                    if (has_post_thumbnail()) {
								                       	the_post_thumbnail('small',
									                       	array(
									                          'class' => 'img-responsive pull-left'
									                        )
									                    ); 
								                    }?>
												</div>
												<div class="gdlr-lms-instructor-title-wrapper">
													<h3 class="gdlr-lms-instructor-title">
														<?=get_the_title();?>
													</h3>
													<div class="gdlr-lms-instructor-position"><?=get_post_meta($q->post->ID, "instructor-position",true);?></div>
												</div>
												<div class="gdlr-lms-author-description"><?=get_post_meta($q->post->ID, "instructor-description",true);?></div>
												<a class="gdlr-lms-button cyan"  href="<?=get_permalink();?>">View Profile</a>
											</div>
											<div class="clear"></div>
										</div>
									</div>
							<?php  }
           						} 
           					wp_reset_postdata();?>     	

							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</section>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php get_footer(); ?>