    <?php wp_footer(); 
    $settings = get_option('clv');?> 
    <footer class="footer-wrapper">
        <div class="footer-container container">
            <?php dynamic_sidebar('footer'); ?>
            <div class="clear"></div>
        </div>           
        <div class="copyright-wrapper">
            <div class="copyright-container container">
                <div class="copyright-left">
                    Theme By LEX             
                </div>
                <div class="copyright-right">
                    Copyright © <?php the_time('Y') ?> - All Right Reserved - LEX              
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.js"></script>
</body>
</html>