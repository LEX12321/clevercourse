<?php get_header(); ?>
<main id="main">
    <div class="section-container container">
        <div class="row">
            <div class="col-md-3">
                <?php get_sidebar() ?>
            </div>
            <div class="col-md-9">
                <?php get_template_part( 'loop' ); ?>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>