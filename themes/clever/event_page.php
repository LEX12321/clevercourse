<?php
/*
Template Name: Event single page 
*/
?>
<?php get_header(); ?>
<div class="content-wrapper">

	<div id="tribe-events-pg-template">
		<div id="tribe-events" class="" data-live_ajax="1" data-datepicker_format="" data-category="" data-featured="">
			<div class="tribe-events-before-html"></div>
			<span class="tribe-events-ajax-loading">
				<img class="tribe-events-spinner-medium" src="Clever%20Course%20Student%20Meeting_files/tribe-loading.gif" alt="Loading Events">
			</span>	
			<div id="tribe-events-content" class="tribe-events-single">

				<p class="tribe-events-back">
					<a href="http://demo.goodlayers.com/clevercourse/events/"> « All Events</a>
				</p>

				<div class="tribe-events-notices">
					<ul>
						<li>This event has passed.</li>
					</ul>
				</div>
				<h1 class="tribe-events-single-event-title">Student Meeting</h1>
				<div class="tribe-events-schedule tribe-clearfix">
					<h2>
						<span class="tribe-event-date-start">October 16, 2014</span> - <span class="tribe-event-date-end">December 17, 2014</span>
					</h2>			
				</div>

				<div id="tribe-events-header" data-title="Student Meeting – Clever Course">

					<h3 class="tribe-events-visuallyhidden">Event Navigation</h3>
					<ul class="tribe-events-sub-nav">
						<li class="tribe-events-nav-previous"></li>
						<li class="tribe-events-nav-next"><a href="#">Teacher Meeting <span>»</span></a></li>
					</ul>

				</div>


				<div id="post-3330" class="post-3330 tribe_events type-tribe_events status-publish has-post-thumbnail hentry tribe_events_cat-meeting cat_meeting">
					<!-- Event featured image, but exclude link -->
					<div class="tribe-events-event-image">
						<img src="Clever%20Course%20Student%20Meeting_files/Fotolia_22324506_Subscription_Monthly_XL.jpg" class="attachment-full size-full wp-post-image" alt="" srcset="http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/uploads/2014/03/Fotolia_22324506_Subscription_Monthly_XL.jpg 1000w, http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/uploads/2014/03/Fotolia_22324506_Subscription_Monthly_XL-300x199.jpg 300w" sizes="(max-width: 1000px) 100vw, 1000px" width="1000" height="664">
					</div>
					<!-- Event content -->
					<div class="tribe-events-single-event-description tribe-events-content">
						
						<p>	Maecenas faucibus mollis interdum. Nulla vitae elit libero, a 
							pharetra augue. Cum sociis natoque penatibus et magnis dis parturient 
							montes, nascetur ridiculus mus. Nullam quis risus eget urna mollis 
							ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl 
							consectetur et. Aenean eu leo quam. Pellentesque ornare sem lacinia quam
							venenatis vestibulum. Maecenas faucibus mollis interdum. Nulla vitae 
							elit libero, a pharetra augue. Morbi leo risus, porta ac consectetur ac,
							vestibulum at eros. Curabitur blandit tempus porttitor. Donec id elit 
							non mi porta gravida at eget metus. Vivamus sagittis lacus vel augue 
							laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque 
							ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla 
							non metus auctor fringilla.&nbsp;
						</p>
						<div class="clear"></div>
						<div class="gdlr-space" style="margin-top: 40px;"></div>
						

							<div class="gdlr-gallery-item gdlr-item">


					  <?php if( have_rows('gallery_item1') ): 
	                        	while ( have_rows('gallery_item1') ) : the_row();
	                    ?>
									<div class="gallery-column three columns">
										<div class="gallery-item" >			

									<?php   $imageID = get_sub_field('gallery_image1');
		                         			$image = wp_get_attachment_image($imageID ,'small');
		                         			$src = wp_get_attachment_image_src($imageID, 'full')[0];
		                         	?>
											<a href="<?=$src;?>"><?=$image;?></a>
											<span class="gallery-caption"><?php the_sub_field('gallery_caption1');?></span>
										</div>
									</div>
						 <?php endwhile;
			                endif; 
			              ?>
							</div>
						<p>Donec id elit non mi porta gravida at eget metus. Vestibulum id 
							ligula porta felis euismod semper. Praesent commodo cursus magna, vel 
							scelerisque nisl consectetur et. Nullam id dolor id nibh ultricies 
							vehicula ut id elit. Vestibulum id ligula porta felis euismod semper. 
							Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis 
							vestibulum. Praesent commodo cursus magna, vel scelerisque nisl 
							consectetur et. Aenean lacinia bibendum nulla sed consectetur. Praesent 
							commodo cursus magna, vel scelerisque nisl consectetur et. Curabitur 
							blandit tempus porttitor. Nullam id dolor id nibh ultricies vehicula ut 
							id elit. Cras mattis consectetur purus sit amet fermentum. Curabitur 
							blandit tempus porttitor. Lorem ipsum dolor sit amet, consectetur 
							adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia 
							quam venenatis vestibulum. Cras justo odio, dapibus ac facilisis in, 
							egestas eget quam. Nullam id dolor id nibh ultricies vehicula ut id 
							elit. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor 
							auctor.</p>
						</div>

						<div class="tribe-events-cal-links">
							<a class="tribe-events-gcal tribe-events-button" href="#">+ Google Calendar</a>
							<a class="tribe-events-ical tribe-events-button" href="#" >+ iCal Export</a>
						</div>				
						</div>
					</div> 
				<div id="tribe-events-footer">
					<h3 class="tribe-events-visuallyhidden">Event Navigation</h3>
					<ul class="tribe-events-sub-nav">
						<li class="tribe-events-nav-previous"></li>
						<li class="tribe-events-nav-next">
						<a href="#">Teacher Meeting <span>»</span></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="tribe-events-after-html"></div>
		</div>
		<!--
		This calendar is powered by The Events Calendar.
		http://m.tri.be/18wn
		-->
	</div>
	<div class="clear"></div>
</div><!-- content wrapper -->
<?php get_footer(); ?>