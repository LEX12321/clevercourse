<?php
/*
Template Name: Сourses page 
*/
?>
<?php get_header(); $settings = get_option('clv');?>
<div class="content-wrapper">
	<div class="gdlr-content">

		<div class="with-sidebar-wrapper">
			<section id="content-section-1">
				<div class="section-container container">
					<div class="instructor-item-wrapper" style="margin-bottom: 30px;">
						<div class="gdlr-lms-instructor-grid-wrapper">
							<div class="clear"></div>
							<?php
							$a =''; $b ='';
							if (isset($_GET['course_category'])){					
					        $a = $_GET["course_category"];
					        }
					        if (isset($_GET['course_type'])){
					        $b =  $_GET["course_type"];
					        }	

							$args_loop = array(
					        'post_type' => 'courses',
					        'course_category' => $a ,
					        'course_type' => $b	        
					        		       
					        ); $i = 0;
					        $q = new WP_Query($args_loop);
					            if($q->have_posts()) {
					              	while($q->have_posts()){ $q->the_post(); $i++ ;
					        ?>
									<div class="gdlr-lms-instructor-grid gdlr-lms-col3">
										<div class="gdlr-lms-item">
											<div class="gdlr-lms-instructor-content">
												<div class="gdlr-lms-instructor-thumbnail">
													<?php
								                    if (has_post_thumbnail()) {
								                       	the_post_thumbnail('small',
									                       	array(
									                          'class' => 'img-responsive pull-left'
									                        )
									                    ); 
								                    }?>
												</div>
												<div class="gdlr-lms-instructor-title-wrapper">
													<h3 class="gdlr-lms-instructor-title">
														<a href="<?=get_permalink();?>"><?=get_the_title();?></a>
													</h3>
													<?php
													$course_category = get_the_terms($post->ID, 'course_category')[0];
													$course_type = get_the_terms($post->ID, 'course_type')[0];
													?>	
													<div class="gdlr-lms-instructor-position"><?php echo ($course_category -> name );?>
													</div>
													
												</div>
												<div class="gdlr-lms-author-description"><?php echo ($course_type -> name );?></div>
												
											</div>
											<div class="clear"></div>
										</div>
									</div>
							<?php if ( $i % 3 == 0) echo '<div class="clear"></div>';
							 }
           						} 
           					wp_reset_postdata();?>     	

							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</section>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php get_footer(); ?>