<?php
/*
Template Name: Сontact page 
*/
?>
<?php get_header(); $settings = get_option('clv');?>
<div class="content-wrapper">
	<div class="gdlr-content">
		<div class="content-wrapper">
			<div class="gdlr-content">

				<div class="above-sidebar-wrapper">
					<section id="content-section-1">
						<div class="gdlr-full-size-wrapper gdlr-show-all" style="padding-bottom: 0px;  background-color: #ffffff; ">
							<div class="gdlr-item gdlr-content-item" style="margin-bottom: 0px;">
							<iframe
							  width="100%"
							  height="450"
							  frameborder="0" style="border:0"
							  src="https://www.google.com/maps/embed/v1/search?key=AIzaSyCAUKr1uihP_2VB8A9MkBksBTHGetxcncU
							  &q=<?=$settings['country'];?>+<?=$settings['city'];?>+<?=$settings['street'];?>
							  &zoom=15
							  &language=en" allowfullscreen>
							</iframe>
							</div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</section>
				</div>


<div class="with-sidebar-wrapper gdlr-type-right-sidebar">
	<div class="with-sidebar-container container">
		<div class="with-sidebar-left eight columns">
			<div class="with-sidebar-content twelve columns">
				<section id="content-section-2">
					<div class="section-container container">
						<div class="gdlr-item gdlr-content-item" style="margin-bottom: 60px;">
							<div class="clear"></div>
							<div class="gdlr-space" style="margin-top: -22px;"></div>
							<h5 class="gdlr-heading-shortcode " style="font-weight: bold;">Talk to us!</h5>
							<div class="clear"></div>
							<div class="gdlr-space" style="margin-top: 45px;"></div>
							<?php echo do_shortcode( '[contact-form-7 id="177" title="Contact form 1"]' ); ?>
						</div>
						<div class="clear"></div>
					</div>
				</section>							
			</div>
			<div class="clear"></div>
		</div>
		<div class="gdlr-sidebar gdlr-right-sidebar four columns">
			<div class="gdlr-item-start-content sidebar-right-item">
				<div id="text-6" class="widget widget_text gdlr-item gdlr-widget">
					<h3 class="gdlr-widget-title">Before Contacting Us</h3>
					<div class="clear"></div>			
					<div class="textwidget"><?=$settings['text1'];?>
					</div>
				</div>
				<div id="text-7" class="widget widget_text gdlr-item gdlr-widget">
					<h3 class="gdlr-widget-title">Contact Information</h3>
					<div class="clear"></div>			
					<div class="textwidget">
						<p><?=$settings['addres'];?></p>
						<p>
							<i class="gdlr-icon icon-phone fa fa-phone" style="color: #444444; font-size: 16px; "></i> <?=$settings['phone'];?>
						</p>
						<p>
							<i class="gdlr-icon icon-envelope fa fa-envelope" style="color: #444444; font-size: 16px; "></i> <?=$settings['email'];?>
						</p>
						<p>
							<i class="gdlr-icon icon-time fa fa-time" style="color: #444444; font-size: 16px; "></i> <?=$settings['schedule'];?>
						</p>
					</div>
				</div>
				<div id="text-8" class="widget widget_text gdlr-item gdlr-widget">
					<h3 class="gdlr-widget-title">Social Media</h3>
					<div class="clear"></div>
					<?php dynamic_sidebar('socialown');?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>				
</div>				


<div class="below-sidebar-wrapper">
	<section id="content-section-3">
		<div class="gdlr-parallax-wrapper gdlr-background-image gdlr-show-all no-skin" id="gdlr-parallax-wrapper-1" data-bgspeed="0.2" style="background-image: url('http://demos-pirftc7xlgm3gz2xr9zm.stackpathdns.com/clevercourse/wp-content/uploads/2013/11/photodune-4503767-skyline-l.jpg'); padding-top: 100px; padding-bottom: 50px; ">
			<div class="container">
				<!-- ACF repeater -->
				<?php
				if( have_rows('contact_label') ): 
					while ( have_rows('contact_label') ) : the_row();?>
					<div class="four columns">
						<div class="gdlr-box-with-icon-ux gdlr-ux" style="opacity: 0; padding-top: 20px; margin-bottom: -20px;">
							<div class="gdlr-item gdlr-box-with-icon-item pos-top type-circle">
								<div class="box-with-circle-icon" style="background-color: #e2714d">
									<?php the_sub_field('icon'); ?><br>
								</div>
								<h4 class="box-with-icon-title"><?php the_sub_field('title'); ?></h4>
								<div class="clear"></div>
								<div class="box-with-icon-caption">
									<p><?php the_sub_field('content'); ?></p>
								</div>
							</div>
						</div>
					</div>
			  <?php endwhile;
				endif; 
				?>

				<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
	</section>
</div>


</div>
<div class="clear"></div>
</div>
<?php get_footer(); ?>