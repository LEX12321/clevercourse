<?php
/*
Template Name: Home page 

*/
?>

<?php get_header(); ?>


<div class="content-wrapper">
	<div class="gdlr-content">
		<div class="with-sidebar-wrapper">
			<section id="content-section-1">
				<div class="gdlr-full-size-wrapper gdlr-show-all" style="padding-bottom: 0px;  background-color: #ffffff; ">
					<div class="gdlr-master-slider-item gdlr-slider-item gdlr-item" style="margin-bottom: 0px;">

						<?php dynamic_sidebar('main'); ?>

					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</section>
		</div>			
	</div>
	<div class="clear"></div>
</div>
<script>
	$(document).ready(function() {
		$('.flexslider').flexslider({
		});
	});
</script>
<?php get_footer(); ?>

