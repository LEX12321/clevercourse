
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?php wp_get_document_title();?></title>
	<?php wp_head();
	$settings = get_option('clv'); ?>
	<!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="body-wrapper  float-menu" data-home="http://demo.goodlayers.com/clevercourse">
			<header class="gdlr-header-wrapper">

				<div class="top-navigation-wrapper">
					<div class="top-navigation-container container">
						<div class="top-navigation-left">   
							<div class="top-navigation-left-text">
								<div style="margin: 0px 10px; display: inline-block; *display: inline; *zoom:1;">
									<i class="gdlr-icon icon-phone fa fa-phone" style="color: #bababa; font-size: 14px; "></i><?=$settings['phone'];?>  
								</div>
								<div style="margin: 0px 10px ; display: inline-block; *display: inline;  *zoom:1;">
									<i class="gdlr-icon icon-envelope fa fa-envelope" style="color: #bababa; font-size: 14px; "></i><a href="mailto:<?=$settings['email'];?>"><?=$settings['email'];?></a>
								</div>                      
							</div>
						</div>
						<div class="top-navigation-right">
							<?php dynamic_sidebar('social'); ?>
							<div class="gdlr-lms-header-signin">
								<i class="fa fa-lock icon-lock"></i>
								<a data-rel="gdlr-lms-lightbox" data-lb-open="login-form">Sign In</a>
								<div class="gdlr-lms-lightbox-container login-form">
									<div class="gdlr-lms-lightbox-close">
										<i class="fa fa-remove icon-remove"></i>
									</div>

									<h3 class="gdlr-lms-lightbox-title">Please sign in first</h3>
									<form class="gdlr-lms-form gdlr-lms-lightbox-form" id="loginform" method="post" action="https://demo.goodlayers.com/clevercourse/wp-login.php">
										<p class="gdlr-lms-half-left">
											<span>Username</span>
											<input name="log" type="text">
										</p>
										<p class="gdlr-lms-half-right">
											<span>Password</span>
											<input name="pwd" type="password">
										</p>
										<div class="clear"></div>
										<p class="gdlr-lms-lost-password">
											<a href="https://demo.goodlayers.com/clevercourse/wp-login.php?action=lostpassword&amp;redirect_to=http://demo.goodlayers.com/clevercourse">Lost Your Password?</a>
										</p>
										<p>
											<input name="home_url" value="http://demo.goodlayers.com/clevercourse" type="hidden">
											<input name="rememberme" value="forever" type="hidden">
											<input name="redirect_to" value="/clevercourse/404error" type="hidden">
											<input name="wp-submit" class="gdlr-lms-button" value="Sign In!" type="submit">
										</p>
									</form>
									<h3 class="gdlr-lms-lightbox-title second-section">Not a member?</h3>
									<div class="gdlr-lms-lightbox-description">Please simply create an account before buying/booking any courses.
									</div>
									<a class="gdlr-lms-button blue" href="http://demo.goodlayers.com/clevercourse">Create an account for free!</a>
								</div>
								<span class="gdlr-separator">|</span>
								<a href="http://demo.goodlayers.com/clevercourse">Sign Up</a>
							</div>                  
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>

				<div class="gdlr-header-inner">
					<div class="gdlr-header-container container">
						<!-- logo -->
						<div class="gdlr-logo">
							<a href="<?php echo get_home_url();?>">
								<?php echo get_custom_logo();
								if (!has_custom_logo()) { ?>
									<h1><?=bloginfo('name');?></h1>
								<?php } ?>							
							</a>
							<div class="gdlr-responsive-navigation dl-menuwrapper" id="gdlr-responsive-navigation">
								<button class="dl-trigger">Open Menu</button>
								<?php wp_nav_menu(	
									$args = array(
									//'theme_location'  => '', // область темы
									//'menu'            => '', // какое меню нужно вставить (по порядку: id, ярлык, имя)
									'container'       => 'false', // блок, в который нужно поместить меню, укажите false, чтобы не 
									'menu_id'         => '', // id меню
									'echo'            => true, // вывести или записать в переменную
									'fallback_cb'     => 'false', // какую функцию использовать если меню не существует, укажите 
									'before'          => '', // текст или html-код, который нужно вставить перед каждым <a>
									'after'           => '', // после </a>
									'link_before'     => '', // текст перед анкором ссылки
									'link_after'      =>'' , // после анкора и перед </a>
									'items_wrap'      => '<ul id="menu-main-menu" class="dl-menu gdlr-main-mobile-menu">%3$s</ul>', 
									'depth'           => 0 // количество уровней вложенности
									)
								);?>
							</div>                      
						</div> 

						<div class="gdlr-navigation-wrapper">
							<nav id="gdlr-main-navigation" class="gdlr-navigation" role="navigation">
								<?php wp_nav_menu(	
									$args = array(
								'theme_location'  => 'primary', // область темы
								'menu'            => '', // какое меню нужно вставить (по порядку: id, ярлык, имя)
								'container'       => 'false', // блок, в который нужно поместить меню, укажите false, чтобы не 
								'menu_id'         => '', // id меню
								'echo'            => true, // вывести или записать в переменную
								'fallback_cb'     => 'false', // какую функцию использовать если меню не существует, укажите false,
								'before'          => '', // текст или html-код, который нужно вставить перед каждым <a>
								'after'           => '', // после </a>
								'link_before'     => '', // текст перед анкором ссылки
								'link_after'      =>'' , // после анкора и перед </a>
								'items_wrap'      => '<ul id="menu-main-menu-1" class="sf-menu gdlr-main-menu">%3$s</ul>', // 
								'depth'           => 0 // количество уровней вложенности
								)
									);
								?>
								<div class="gdlr-nav-search-form-button" id="gdlr-nav-search-form-button">
									<i class="fa fa-search icon-search"></i>
								</div>
								<div class="clear"></div> 
							</nav>
						</div>  

						<div class="clear"></div>
					</div>	
				</div>
			</header>

			<div id="gdlr-header-substitute"></div>
			<div class="gdlr-nav-search-form" id="gdlr-nav-search-form">
				<div class="gdlr-nav-search-container container"> 
					<form method="get" action="<?php echo esc_url(home_url('/')); ?>">
						<i class="fa fa-search icon-search"></i>
						<input id="searchsubmit" class="style-2" value="" type="submit">
						<div class="search-text" id="search-text">
							<input value="Type keywords..." name="s" id="s" autocomplete="off" data-default="Type keywords..." type="text">
						</div>
						<div class="clear"></div>
					</form>
				</div>
			</div>	

			<? if( !is_page( array('home','contact-page','single') ) ):?>
			<div class="gdlr-page-title-wrapper">
				<div class="gdlr-page-title-overlay"></div>
				<div class="gdlr-page-title-container container">
		  <?php if ( is_page ('404 Page')||is_404()){ ?>
					<h1 class="gdlr-page-title">404 </h1>
					<span class="gdlr-page-caption">Page not found</span>
		  <?php } else { ?>
		  			<h1 class="gdlr-page-title"><?=wp_title('',true); }?></h1>
				</div>	
			</div>	
			<? endif;	
