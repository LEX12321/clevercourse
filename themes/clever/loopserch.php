<?php if (have_posts()) : ?>


    <?php while (have_posts()) : the_post(); ?>    
        <div class="gdlr-lms-instructor-grid gdlr-lms-col3">
            <div class="gdlr-lms-item">
                <div class="gdlr-lms-instructor-content">
                    <div class="gdlr-lms-instructor-thumbnail">
                        <?php
                        if (has_post_thumbnail()) {
                            the_post_thumbnail('small',
                                array(
                                  'class' => 'img-responsive pull-left'
                                  )
                                ); 
                            }?>
                        </div>
                        <div class="gdlr-lms-instructor-title-wrapper">
                            <h3 class="gdlr-lms-instructor-title">
                                <a href="<?=get_permalink();?>"><?=get_the_title();?></a>
                            </h3>
                            <?php
                            $course_category = get_the_terms($post->ID, 'course_category')[0];
                            $course_type = get_the_terms($post->ID, 'course_type')[0];
                            ?>  
                            <div class="gdlr-lms-instructor-position"><?php echo ($course_category -> name );?>
                            </div>   
                        </div>
                        <div class="gdlr-lms-author-description"><?php echo ($course_type -> name );?></div>  
                    </div>
                    <div class="clear"></div>
                </div>
                </div>
    <?php endwhile; ?>
<?php endif; ?>