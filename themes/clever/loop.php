<?php if (have_posts()) : ?>


    <?php while (have_posts()) : the_post(); ?>    
        <h2>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h2>
        <p class="lead">
            <?php echo __('by','sg')?> <a href="index.php"><?php the_author() ?></a>
        </p>
        <p><span class="glyphicon glyphicon-time"></span>  <?php echo __('Posted on  ','sg')?>  <?php the_time('M.d, Y H.m') ?></p>
        <hr>
        <?php if (has_post_thumbnail()) { ?>
            <?php the_post_thumbnail('medium',
             array(
             		'class' => 'img-responsive'
             		)); ?>
            <!-- <hr> -->
        <?php } ?>
        <?php
        $course_category = get_the_terms($post->ID, 'course_category')[0];
        $course_type = get_the_terms($post->ID, 'course_type')[0];
        ?>  
        <div class="gdlr-lms-instructor-position"><?php echo ($course_category -> name );?></div>
        
    
    <div class="gdlr-lms-author-description"><?php echo ($course_type -> name );?></div>
        <?php the_content(); ?>
        <!-- <a class="btn btn-primary" href="<?php the_permalink(); ?>"><?php echo __('Read More','sg')?> <span class="glyphicon glyphicon-chevron-right"></span></a> -->

       <!--  <hr> -->
        
        <!-- the_permalink();

        the_excerpt();

        the_post_thumbnail();

        the_time(); -->

    <?php endwhile; ?>
<?php endif; ?>