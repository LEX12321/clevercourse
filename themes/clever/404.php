<?php
/*
Template Name: 404 page 
*/
?>
<?php get_header(); ?>

<div class="content-wrapper">
	<div class="page-not-found-container container">
		<div class="gdlr-item page-not-found-item">
			<div class="page-not-found-block">
				<div class="page-not-found-icon">
					<i class="fa fa-frown-o icon-frown"></i>
				</div>
				<div class="page-not-found-title">
					Error 404				
				</div>
				<div class="page-not-found-caption">
					Sorry, we couldn't find the page you're looking for.				
				</div>
				<div class="page-not-found-search">
					<div class="gdl-search-form">
						<form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
							<div class="search-text" id="search-text">
								<input name="s" id="s" autocomplete="off" data-default="Type keywords..." value="Type keywords..." type="text">
							</div>
							<input id="searchsubmit" value="" type="submit">
							<div class="clear"></div>
						</form>
					</div>				
				</div>
			</div>
		</div>
	</div>

	<div class="clear">
		
	</div>
</div><!-- content wrapper -->

<?php get_footer(); ?>