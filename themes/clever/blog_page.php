<?php
/*
Template Name: Blog page 
*/

get_header(); ?>
<div class="content-wrapper">
	<div class="gdlr-content">
		<div class="with-sidebar-wrapper">
			<section id="content-section-1">
				<div class="section-container container">
					<div class="blog-item-wrapper">
						<div class="blog-item-holder">
							<div class="gdlr-isotope" data-type="blog" data-layout="fitRows">
								<div class="clear"></div>
									<?php
									$args_loop = array(
						        	'post_type' => 'post',
						        	'category_name' => 'blog' 
						        	);
									$i = 0;
						        	$q = new WP_Query($args_loop);
						            if($q->have_posts()) {
						              	while($q->have_posts()){ $q->the_post(); $i++ ;

						        	?>
						        	<div class="four columns">
							        	<div class="gdlr-item gdlr-blog-grid">
							        		<div class="gdlr-ux gdlr-blog-grid-ux">
							        			<article class="post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-fit-row category-life-style tag-blog tag-life-style">
							        				<div class="gdlr-standard-style">
							        					<div class="gdlr-blog-thumbnail">
							        						<a href="<?=the_permalink();?>"> 
							        							<?php
							        							if (has_post_thumbnail()) {
							        								the_post_thumbnail('blog',
							        									array(
							        										
						        										)
						        									); 
						        								}?>
						        							</a>
															<!-- <div class="gdlr-sticky-banner"><i class="fa fa-bullhorn icon-bullhorn"></i>Sticky Post
														</div> -->		
														</div>

														<header class="post-header">
															<h3 class="gdlr-blog-title">
																<a href="<?=the_permalink();?>"><?=the_title();?></a>
															</h3>

															<div class="gdlr-blog-info gdlr-info">
																<div class="blog-info blog-date">
																	<span class="gdlr-head">Posted on</span>
																	<a href="http://demo.goodlayers.com/clevercourse/2014/01/06/"><?php echo date('j M Y',strtotime(get_the_date()));?></a>
																</div>
																<div class="blog-info blog-tag">
																<span class="gdlr-head">Tags</span>
																<?php the_tags('',', ',''); ?>
																</div>
																<div class="blog-info blog-comment"><span class="gdlr-head">Comments</span><a href="<?php the_permalink() ?>comments">
																	<?php comments_number('0', '1', '%'); ?>
																</a>
																</div>
																<div class="clear"></div>
															</div>	
															<div class="clear"></div>
														</header>

														<div class="gdlr-blog-content"><?=the_excerpt();?> 
															<div class="clear"></div>
															<a href="<?=the_permalink();?>" style="margin-top:5px;" class="gdlr-button with-border excerpt-read-more">Read More</a>
														</div>	
													</div>
												</article>
											</div>
										</div>
									</div>
									<?php 
										if ( $i % 3 == 0) echo '<div class="clear"></div>';
										 }
										}  
									wp_reset_postdata();?>

								<div class="clear"></div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</section>
		</div>		
	</div>
	<div class="clear"></div>
	
</div>
	<?php get_footer(); ?>